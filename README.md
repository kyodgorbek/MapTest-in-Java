# MapTest-in-Java



import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapTest {
  
   public static void main(String[] args){
      
      Map favoriteColors = new HashMap();
      favoriteColors.put("Juliet", Color.pink);
      favoriteColors.put("Romeo", Color.green);
      favoriteColors.put("Adam", Color.blue);
      favoriteColors.put("Eve", Color.pink);
    }
    
    private static void print(Map m)
    {
        Set keySet = m.keySet();
        Iterator iter = keySet.iterator();
        while (iter.hasNext())
        {
           Object key = iter.next();
           Object value = m.get(key);
           System.out.println(key + "->" + value);
        }  
      }
    }    
